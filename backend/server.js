const express=require("express")
const cors=require('cors')
const db=require('../db')
const utils =require('./utils')
const router=require('./routes')

const app=express()
app.use(cors('*'))
app.use(express.json())



app.get('/',(request,response) => {
const query=`select empid,empname,salary,age from emp`

db.execute(query,(error,result) => {

    response.send(utils.createResult(error,result))
})

})

    app.post('/',(request,response) => {
    const {empid,empname,salary,age} = request.body

    const query=`insert into emp (empid,empname,salary,age) values ('${empid}','${empname}','${salary}','${age}') `

    db.execute(query,(error,result) => {
    
        response.send(utils.createResult(error,result))
    })
    
    })
    
    app.put('/:id',(request,response) => {
    const { id } = request.params
    const {empid,empname,salary,age} = request.body

    const query=`update emp set salary='${salary}' where empid=${id}`


        
    db.execute(query,(error,result) => {
    
        response.send(utils.createResult(error,result))
    })
    
    })

    app.delete('/:id',(request,response) => {
        const { id } = request.params
       
    
        const query=`delete from emp where empid=${id}`
        
    
            
        db.execute(query,(error,result) => {
        
            response.send(utils.createResult(error,result))
        })
        
    })

    

app.listen(4000,() => {
    console.log('server strated at 4000')
})

